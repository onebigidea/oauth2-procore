# Procore Provider for OAuth 2.0 Client

This package provides Procore OAuth 2.0 support for the PHP League's [OAuth 2.0 Client](https://github.com/thephpleague/oauth2-client).

## Installation

To install, use composer:

```
composer require oxblue/oauth2-procore
```

## Usage

Usage is the same as The League's OAuth client, using `\OxBlue\OAuth2\Client\Provider\Procore` as the provider.

### Authorization Code Flow

```php
session_start();

// You can also use a headless redirect URI by using the following as the
// redirectUri: `OxBlue\OAuth2\Client\Provider\Procore::HEADLESS_REDIRECT_URI`
$provider = new OxBlue\OAuth2\Client\Provider\Procore([
    'clientId'          => '{procore-client-id}',
    'clientSecret'      => '{procore-client-secret}',
    'redirectUri'       => 'https://example.com/callback-url'
]);

if (!isset($_GET['code'])) {

    // If we don't have an authorization code then get one
    $authUrl = $provider->getAuthorizationUrl();
    $_SESSION['oauth2state'] = $provider->getState();
    header('Location: '.$authUrl);
    exit;

// Check given state against previously stored one to mitigate CSRF attack
} elseif (empty($_GET['state']) || ($_GET['state'] !== $_SESSION['oauth2state'])) {

    unset($_SESSION['oauth2state']);
    exit('Invalid state');

} else {

    // Try to get an access token (using the authorization code grant)
    $token = $provider->getAccessToken('authorization_code', [
        'code' => $_GET['code']
    ]);

    // Optional: Now you have a token you can look up a users profile data
    try {

        // We got an access token, let's now get the user's details
        $user = $provider->getResourceOwner($token);

        // Use these details to create a new profile
        printf('Hello %s!', $user->getUsername());

    } catch (Exception $e) {

        // Failed to get user details
        echo "Whoops, we couldn't get user details.";
        exit(1);
    }

    // Use this to interact with an API on the users behalf
    echo $token->getToken();
}
```

## Testing

``` bash
$ ./vendor/bin/phpunit
```
